﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Controller : MonoBehaviour {

	NavMeshAgent agent;
	// Use this for initialization
	public float enemyHealth;
	public int damagePoints;

	void Start () {
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		agent.SetDestination (GameObject.FindWithTag ("Player").transform.position);
        if (enemyHealth <= 0)
        {
			Manager_EndLevel.numEnemies--;
			Destroy (this.gameObject);
		}
	}

	public void OnCollisionEnter(Collision other){
		if (other.transform.CompareTag ("Player")) {
			GameManager.instance.TakeHealth (damagePoints);
		}
	}
}
