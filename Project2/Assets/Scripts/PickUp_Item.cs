﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_Item : MonoBehaviour
{
    public GameObject pickedUpItem;
    public string pickedUpItemName;
	public bool firstTry;
    // Use this for initialization
    void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {

		if(other.CompareTag("Item"))
        {
            if (other.GetComponent<Item>().canPickUp)
            {
                if (other.GetComponent<Item>().stackable == false)
                {
                    if (InventoryManager.instance.ItemExistInList(other.gameObject) == false || firstTry)
                    {
                        firstTry = false;
                        pickedUpItemName = other.gameObject.transform.name;
                        pickedUpItem = other.gameObject;

                        InventoryManager.instance.AddItem(other.gameObject, other.GetComponent<Item>().stackable);
                        other.gameObject.SetActive(false);

                    }
                }
                else
                {
                    firstTry = false;
                    pickedUpItemName = other.gameObject.transform.name;
                    pickedUpItem = other.gameObject;

                    InventoryManager.instance.AddItem(other.gameObject, other.GetComponent<Item>().stackable);
                    other.gameObject.SetActive(false);
                }
            }
			Debug.Log ("Collide");
		}

    }
}
