﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_BasicHandgun : MonoBehaviour {

	public float cooldownTime;
	public GameObject handgunBullet;
	float currentCooldown;
    public AudioClip handgunSound;

    public Ui_Controller uiController;

    // Use this for initialization
    void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
        uiController.UpdateAmmo("Ammo: Unlimited");
		CheckForFireButton ();
		currentCooldown = Mathf.Max ((currentCooldown - (1 * Time.deltaTime)), 0);
		//Debug.Log (currentCooldown);
	}
	public void CheckForFireButton()
	{
		if (Input.GetMouseButtonDown (0) && CheckForCooldown()) 
		{
            AudioManager.instance.PlaySoundEffect(handgunSound);
			Debug.Log ("Fire");
			Instantiate (handgunBullet, transform.position, transform.rotation);
			currentCooldown = cooldownTime;
		}
	}

	public bool CheckForCooldown()
	{
		if (currentCooldown <= 0) 
		{
			currentCooldown = cooldownTime;
			return true;
		} 
		else 
		{
			return false;
		}
	}
}
