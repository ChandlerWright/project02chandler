﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Bazooka : MonoBehaviour {

	public GameObject explosionField;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider other){
		Instantiate (explosionField, transform.position, Quaternion.identity);
		Destroy (this.gameObject);
	}

}
