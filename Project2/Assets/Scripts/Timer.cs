﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float totalTime;
    public float currentTime;
    //public string currentLevel;

    public Ui_Controller uiController;

    // Use this for initialization
    void Start()
    {
        //set curretTime to totalTime
        currentTime = totalTime;
    }

    // Update is called once per frame
    void Update()
    {
        //minus currentTime from the time
        currentTime -= Time.deltaTime;

        //if currentTime <= 0
        if (currentTime <= 0)
        {
            /////restart level
            GameManager.instance.TakeLives(1);
            //set currentTime to -1
            currentTime = -1;
        }

        uiController.UpdateTimer(string.Format("Time Left: {0}", Mathf.Round(currentTime)));
    }
}
